if(NOT DEFINED mipc_lib_deps_list)
  set(mipc_lib_deps_list "")
endif()

macro(mipc_add_dep depname)
  if(NOT "${${depname}_LIBRARIES}")
    set(${depname}_LIBRARIES ${depname})
  endif()

  message(STATUS "Adding mipc dependency ${${depname}_LIBRARIES} [triggered by ${depname}]")

  list(APPEND mipc_lib_deps_list ${${depname}_LIBRARIES})
endmacro()

macro(mipc_export_deps)
  set(mipc_lib_deps "")
  list(JOIN mipc_lib_deps_list " -l" mipc_lib_deps)
  list(LENGTH mipc_lib_deps_list deps_count)
  if ("${deps_count}" GREATER "0")
    string(PREPEND mipc_lib_deps "-l")
  endif()
  message(STATUS "Exporting mipc deps [${mipc_lib_deps}] into mipc_lib_deps")
endmacro()
