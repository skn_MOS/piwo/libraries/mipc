# MIPC PROGRAMMING GUIDELINES
##### Standard library
For file I/O - stdio from C standard library should be used instead of streams.
For debug I/O - to stdout / stderr api from debug_print should be used.
This will allow to enable / disable logging when compiling library.

#### Code formatting
Code should be formated using clang-format with -style=file option and
.clang-format file from projects main directory ought to me used.

#### Platform dependent code
Code that is platform dependent should be enclosed inside ifdef / elif
directives. Those preprocesor directives should not be indented. And the Order
of platform dependent implementation in such block is defined as follows:

1. Windows 32 and 64 bit (required)
2. Windows 32 bit (only if needed)
3. Windows 64 bit (only if needed)
4. Linux (required)
5. Linux 32 bit (only if needed)
6. Linux 64 bit (only if needed)

So distinction between 32 and 64 bit version does not need to be present.
However, one cannot exist without the other. So there is either no
distinction for word width (bit-ness) version or there are both.
The above rule holds only for given OS. It means that it is permitted
to distinct between Windows 32 and Windows 64 bit and at the same time
define only general Linux version for both 32 and 64 bits.

##### Platform dependend files
Files containing definitions of system function wrappers should be placed \
in `include/platform/<platform-name>/` for include files and \
in `source/platform/<platform-name>/` for source files.

#### Comments
All comments should be valid English sentences i.e. start with capital
letter and have proper punctuation marks. The only exception to the above
rule are onclose namespace comments and when writing short
(one line or few words) comments to simply hint something in line below
the comment. Author of comment can use all capital letters to write about
crucial / tricky warrnings that requires additional attention of the reader.

#### Debug prints
For debug logging debug_print* functions / macros should be used from
debug_print.h header. If some arguments being printed within debug_print
to help debugging, caller should make sure that printed arguments are safe
to print, eg. when printing string argument make sure to explicitly pass
maximum length of it in case of unterminated string by passing format
string like ("Argument is bad: %30s, expected 'some string'", strarg)

#### Unsafe functions / methods
If implementation of some functions can be optimized when some
assumptions about passed args can be made, so extra args checking can be
omitted, developer is allowed to provide such function. Functions that 
make some assumptions about passed arguments should be therefore prefixed
with single underscore to indicate that caller should pay extra caution when
calling this api.
example:
``` cpp
void foo(int param);  // safe implementation with runtime checking
void _foo(int param); // unsafe, faster implementation, call only when you are
                      // sure what u are doing
```

#### TODOs
It is permitted to indicate with proper comment that something has to be done
in (non distant) future regarding some code block, for example when providing
some safe, easy to write, but slower implementation of some function can be
marked in special way to indicate that optimization can be (should be) done.
Such marking can be achived using comment starting with "TODO(<name>)", where
<name> is name (or nick name, whatever) of person that is responsible for this
todo.
Example:
``` cpp

void sum_to_n(int n)
{
  auto sum = 0;

  // TODO(holz) solve this with formula for sum of arithmetic series
  for(auto i = 0; i < n; i++) {
    sum += i;
  }

  return sum;
}

```
Such comment indicated for user 'holz' that he should rewrite below loop,
so that it uses formula for arithmetic series.

##### Note: To indicate that TODO is meant for anybody, simply insert TODO(all)
