#include "debug_print.h"
#include "error.h"
#include "mipc.h"
#include "permission.h"
#include "shmem_utils.h"
#include "utils.h"

#include <charconv>
#include <cstddef>
#include <cstdio>
#include <inttypes.h>

#ifdef __WIN32

#elif __linux__

#include <cerrno>
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <unistd.h>

#endif

mipc::shmem::shmem_t
mipc::shmem::shmem_t::create(const char *name,
                             const char *config_file_name) noexcept
{
  mmapping_t config_file = mmapping_t::map(config_file_name, perm_t::RDWR);

  if (!config_file.is_valid()) {

    debug_printerr("Cannot open config file: %s\n", config_file_name);
    return {};
  }

  return create_(name, config_file);
}

mipc::shmem::shmem_t
mipc::shmem::shmem_t::create_(const char *name,
                              mmapping_t &config_file) noexcept
{
  return create_(name,
                 reinterpret_cast<char *>(config_file.begin()),
                 config_file.get_size());
}

mipc::shmem::shmem_t
mipc::shmem::shmem_t::create_(const char *name,
                              const char *header_data,
                              size_t header_size) noexcept
{
  meminfo_t meminfo =
    utils::json_parse_from_memory(header_data, header_size);

  if (!meminfo["size"].IsUint64()) {

    debug_printerr("Shared memory size field is not a valid int!\n");
    return {};
  }

  size_t shmem_size = meminfo["size"].GetUint64();

  if (header_size >= shmem_size) {

    debug_printerr(
      "Shared memory size declared in config doesnt make sense! "
      "Declared shared mem size is smaller than config itself.\n");

    return {};
  }

  debug_print("Creating shared memory with size %zu and name %s.\n",
              shmem_size,
              name);

  size_t total_shmem_size = shmem_size + header_size;
  // TODO(holz) Dont hardcode accmod, add way for user to choose mode
  shmem_native_t fd = shmem_native_t::create(
    name, total_shmem_size, perm_t::RDWR, perm_t::RDWR);

  if (!fd.is_valid()) {

    debug_printerr("Creating shared memory %s failed! "
                   "with code: %d\n",
                   name,
                   errno);

    return {};
  }

  debug_print("Memory successfully opened %s\n", name);

  mmapping_t mmapping =
    mmapping_t::map(fd.get_fd(), total_shmem_size, perm_t::RDWR);

  if (!mmapping.is_valid()) {

    debug_printerr("Mapping shared memory failed!\n");

    return {};
  }

  // Write json to the beggining of the memory
  std::copy(header_data,
            header_data + header_size,
            reinterpret_cast<char *>(mmapping.begin()));

#ifdef __WIN32

  return {
    std::move(meminfo), std::move(mmapping), std::move(fd), header_size
  };

#elif __linux__

  return { std::move(meminfo),
           std::move(mmapping),
           std::string("/dev/shm/") + name,
           header_size };

#endif
}

mipc::shmem::shmem_t
mipc::shmem::shmem_t::open(const char *name) noexcept
{
  shmem_native_t fd = shmem_native_t::open(name, perm_t::RDWR);

  if (!fd.is_valid()) {

    debug_printerr("Opening shared memory %s failed! "
                   "with code: %d\n",
                   name,
                   errno);

    return {};
  }

  size_t shmem_size = utils::get_file_size(fd.get_fd());

  debug_printerr("Mapping created shared memory.\n");
  mmapping_t mmapping =
    mmapping_t::map(fd.get_fd(), shmem_size, perm_t::RDWR);

  if (!mmapping.is_valid()) {

    debug_printerr("Mapping shared memory failed! Code: " PRId64 "\n",
                   utils::get_last_system_error());

    return {};
  }

  // TODO(holz) perhaps there is better way of getting this size, for
  // example directly from rapidjson parsing function. It parses whole json
  // so it must known where it ends.
  size_t header_size =
    utils::get_json_size<size_t>(mmapping.begin(), mmapping.end());

  debug_printerr("Mapping successfull!.\n");
  meminfo_t meminfo =
    utils::json_parse_from_memory(mmapping.begin(), header_size);

  return { std::move(meminfo), std::move(mmapping), header_size };
}

void
mipc::shmem::shmem_t::close() noexcept
{
  this->mmapping.unmap();

#ifdef __WIN32
  // This is done automatically by os
#elif __linux__
  this->shobj.unlink();
#endif
}
