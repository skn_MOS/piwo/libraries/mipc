#include "base.h"

#include <cstdio>

#ifdef __WIN32

#include <cstdlib>

void
forgot_call_init()
{
  fprintf(stderr, "Error! Forgot to call mipc::init().\n");
  std::abort();
}

NTQUERYSECTIONPT NtQuerySection = (NTQUERYSECTIONPT)forgot_call_init;

void
mipc::init()
{
  NtQuerySection = (NTQUERYSECTIONPT)GetProcAddress(
    LoadLibrary("ntdll.dll"), "NtQuerySection");

  if (!NtQuerySection) {
    fprintf(stderr, "Failed to load NtQuerySection. Aborting.\n");
    exit(7);
  }

  printf("Successfully loaded nt procs.\n");
  printf("NtQuerySection %p.\n", (void*)NtQuerySection);
}

#elif __linux__

#include <sys/stat.h>

void
mipc::init()
{
  umask(0);
}

#endif
