#include "mapping.h"
#include "debug_print.h"
#include "file_native.h"
#include "permission.h"
#include "shmem_utils.h"
#include "utils.h"

#include <inttypes.h>

#if defined(__WIN32)
#include "fs.h"
#endif

mipc::shmem::mmapping_t::mmapping_t() noexcept
  : addr{ INVALID_MAP }
{}

mipc::shmem::mmapping_t::mmapping_t(std::byte *addr, size_t size) noexcept
  : addr{ addr }
  , size{ size }
{}

mipc::shmem::mmapping_t::mmapping_t(mmapping_t &&other) noexcept
  : addr{ other.addr }
  , size{ other.size }
{
  other.addr = INVALID_MAP;
}

void
mipc::shmem::mmapping_t::operator=(mmapping_t &&rhs) noexcept
{
  this->unmap();

  this->addr = rhs.addr;
  this->size = rhs.size;

  rhs.addr = INVALID_MAP;
}

mipc::shmem::mmapping_t::~mmapping_t() noexcept
{
  this->unmap();
}

mipc::shmem::mmapping_t
mipc::shmem::mmapping_t::map(const char *file_name, perm_t mode) noexcept
{
#ifdef __WIN32

  fd_native_t fd = host_open(file_name, mode);

  if (!fd_valid(fd)) {
    debug_printerr("Failed to open file %s with code" PRId64 "\n",
                   file_name,
                   GetLastError());
    return {};
  }

  debug_printerr("File %s opened with descriptor %d\n", file_name, fd);

  DWORD file_size_high;
  DWORD file_size_low = GetFileSize(fd, &file_size_high);

  if (file_size_low == INVALID_FILE_SIZE) {
    debug_printerr("Got invalid size of file! %ld\n", file_size_low);
    CloseHandle(fd);
    return {};
  }

  fd_native_t fdm = CreateFileMapping(
    fd, NULL, perm2pageprot(mode), file_size_high, file_size_low, NULL);

  if (fdm == NULL) {
    debug_printerr(
      "Failed to CrateFileMapping from opened file with error " PRId64 "\n",
      GetLastError());
    CloseHandle(fd);
    return {};
  }

  SIZE_T file_size = (file_size_high << sizeof(DWORD)) | file_size_low;
  fd_native_t fd_view =
    MapViewOfFile(fdm, perm2mapacc(mode), 0, 0, file_size);

  CloseHandle(fd);

  if (fd_view == NULL) {
    debug_printerr("MapViewOfFile failed with error: " PRId64 "\n",
                   GetLastError());
    return {};
  }

  debug_print("Successfully created map fiew at: %p and size: %zu\n",
              fd_view,
              file_size);
  return { reinterpret_cast<std::byte *>(fd_view), file_size };

#elif __linux__

  fd_native_t fd = host_open(file_name, mode);

  if (!fd_valid(fd)) {
    debug_printerr("Failed to open file %s\n", file_name);
    return {};
  }

  size_t file_size = utils::get_file_size(fd);

  mmapping_t map = mmapping_t::map(fd, file_size, mode);

  host_close(fd);

  return map;

#endif
}

mipc::shmem::mmapping_t
mipc::shmem::mmapping_t::map(fd_native_t shmem_fd,
                             size_t size,
                             perm_t mode) noexcept
{
  void *ret = host_map(shmem_fd, size, mode);

#ifdef __WIN32
  debug_print("mapping_object %p, addr %p\n", shmem_fd, ret);
#elif __linux__
  debug_print("mapping_object %d, addr %p\n", shmem_fd, ret);
#endif

  return { static_cast<std::byte *>(ret), size };
}

void
mipc::shmem::mmapping_t::unmap() noexcept
{
  debug_printerr("Checking if current mapping is valid.\n");

  if (!this->is_valid()) {
    debug_printerr("Mapping not valid. Doing nothing.\n");
    return;
  }

  debug_printerr("Mapping is valid, calling unmap.\n");
  mipc::host_unmap(this->addr, this->size);

  this->addr = INVALID_MAP;
}
