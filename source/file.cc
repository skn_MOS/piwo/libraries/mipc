#include "file.h"

#include "file_native.h"
#include "fsio.h"
#include "mapping.h"

namespace mipc {
  void
  finbuf::free_buffer() const noexcept
  {
    if (this->_data == nullptr)
      return;

    if (this->_size <= MAX_BUFFER_SIZE)
      delete[] this->_data;
    else
      mipc::host_unmap((void *)this->_data, this->_size);
  }

  finbuf::finbuf(shmem::mmapping_t &&mapping)
  {
    this->_data = reinterpret_cast<const char *>(mapping.begin());
    this->_size = mapping.get_size();

    mapping.release_();
  }

  finbuf::finbuf(const char *filename)
  {
    constexpr mipc::perm_t perm = mipc::perm_t::RDONLY;
    mipc::fd_native_t fd = mipc::host_open(filename, perm);

    if (!mipc::fd_valid(fd)) {
      this->def_init();
      return;
    }

    size_t filesize = mipc::host_filesize(fd);

    if (filesize == 0) {
      mipc::host_close(fd);
      this->def_init();
      return;
    }

    if (filesize <= MAX_BUFFER_SIZE) {
      auto buffer = new char[filesize];
      this->_size = mipc::host_read_all(fd, buffer, filesize);
      this->_data = buffer;
    } else {
      // TODO(holz): closing and reopening this file is a joke.
      // Create function for creating mmapping from file descriptor
      mipc::host_close(fd);
      *this = shmem::mmapping_t::map(filename, perm);
    }

    mipc::host_close(fd);
  }
} // namespace mipc
