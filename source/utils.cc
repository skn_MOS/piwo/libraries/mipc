#include "utils.h"

#if defined(GetObject)
#  undef GetObject
#endif

#include "rapidjson/filereadstream.h"

bool
mipc::utils::starts_with(std::string_view s, std::string_view prefix)
{
  if (s.size() < prefix.size())
    return false;

  for (size_t i = 0; i < prefix.size(); ++i) {
    if (s[i] != prefix[i])
      return false;
  }

  return true;
}
