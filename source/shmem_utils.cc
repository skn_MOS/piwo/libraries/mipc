#include "shmem_utils.h"


#ifdef __WIN32

#include "base.h"
#include "wntapi.h"

#elif __linux__

#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#endif

mipc::shmem::shmem_t::meminfo_t
mipc::utils::json_parse_from_memory(const void *memory)
{
  rapidjson::Document ret;
  ret.Parse(reinterpret_cast<const char *>(memory));

  return ret;
}

mipc::shmem::shmem_t::meminfo_t
mipc::utils::json_parse_from_memory(const void *memory, size_t length)
{
  rapidjson::Document ret;
  ret.Parse(reinterpret_cast<const char *>(memory), length);

  return ret;
}

#ifdef __WIN32

size_t
mipc::utils::get_file_size(mipc::fd_native_t fd) noexcept
{
  SECTION_BASIC_INFORMATION secinfo = {};

  NTSTATUS status = NtQuerySection(
    fd, SectionBasicInformation, &secinfo, sizeof(secinfo), 0);

  fprintf(stderr,
             "Got file size. Status: 0x%lX Size: %lld\n",
             status,
             secinfo.Size.QuadPart);

  return secinfo.Size.QuadPart;
}

#elif __linux__

size_t
mipc::utils::get_file_size(mipc::fd_native_t fd) noexcept
{
  struct stat stats;

  if (fstat(fd, &stats) == -1) {
    return 0;
  }

  return stats.st_size;
}

#endif

