#include "pipe_listener.h"
#include "debug_print.h"
#include "utils.h"

#include <cstring>
#include <string>

namespace mipc::pipe {

  listener_t::listener_t(listener_t &&conn)
    : fd(conn.fd)
#ifdef __WIN32
    , pipe_name(std::move(conn.pipe_name))
#endif
  {
    debug_printerr("Moving conn\n");
    conn.fd = INVALID_FD;
  }

  void
  listener_t::operator=(listener_t &&conn)
  {
    debug_printerr("Checking if fd valid\n");

    if (mipc::fd_valid(this->fd)) {
      debug_printerr("Valid, closing.\n");
      mipc::host_close(this->fd);
    } else {
      debug_printerr("Not valid, doing nothing.\n");
    }

    this->fd = conn.fd;
    conn.fd = INVALID_FD;

#ifdef __WIN32
    this->pipe_name = conn.pipe_name;
#endif
  }

  listener_t::~listener_t() noexcept
  {
    debug_printerr("Checking if fd valid\n");

    if (!mipc::fd_valid(this->fd)) {
      debug_printerr("Not valid, doing nothing.\n");
      return;
    }

    debug_printerr("Valid, closing.\n");
    mipc::host_close(this->fd);
  }

  conn_t
  listener_t::accept()
  {
    return conn_t::_create_from(this->host_accept());
  }

#ifdef __WIN32

  listener_t
  listener_t::create(const char *name, int max_con)
  {
    const int name_len = std::strlen(name);
    const int size = name_len + system_pipe_path.size();

    std::string full_name;

    full_name.reserve(size);
    full_name = system_pipe_path.data();
    full_name.append(name, name_len);

    debug_printerr("Opening pipe with name %s (size:%d)\n",
                   full_name.c_str(),
                   full_name.size());

    listener_t ret{ host_create(full_name, max_con) };
    ret.pipe_name = std::move(full_name);

    return ret;
  }

  fd_native_t
  listener_t::host_create(const std::string &name, unsigned max_con)
  {
    constexpr DWORD pipe_access =
      PIPE_ACCESS_DUPLEX | FILE_FLAG_OVERLAPPED;
    constexpr DWORD pipe_type = PIPE_TYPE_BYTE | PIPE_READMODE_BYTE |
                                PIPE_WAIT | PIPE_REJECT_REMOTE_CLIENTS;
    constexpr DWORD in_buffer_size = 8192;
    constexpr DWORD out_buffer_size = 8192;
    constexpr DWORD def_timeout = 50;

    return CreateNamedPipe(name.c_str(),
                           pipe_access,
                           pipe_type,
                           max_con,
                           in_buffer_size,
                           out_buffer_size,
                           def_timeout,
                           NULL);
  }

  static fd_native_t
  host_accept_(fd_native_t fd)
  {
    HANDLE ret = INVALID_FD;

    if (ConnectNamedPipe(fd, NULL)) {
      if (!DuplicateHandle(GetCurrentProcess(),
                           fd,
                           GetCurrentProcess(),
                           &ret,
                           0,
                           FALSE,
                           DUPLICATE_SAME_ACCESS)) {

        debug_printerr("FAILED TO DUPLICATE HADNLE!\n");
      }
    }

    return ret;
  }

  fd_native_t
  listener_t::host_accept()
  {
    debug_printerr("Waiting for client to connect\n");

    fd_native_t client_fd = host_accept_(this->fd);

    if (!fd_valid(client_fd)) {
      auto last_error = GetLastError();

      // Recreate server and retry
      if (last_error == ERROR_PIPE_NOT_CONNECTED) {
        if (this->is_valid())
          mipc::host_close(this->fd);

        this->fd = host_create(this->pipe_name.c_str());

        if (LIKELY(fd_valid(this->fd)))
          client_fd = host_accept_(this->fd);
      }
    }

    return client_fd;
  }

#elif __linux__

  listener_t
  listener_t::create(const char *name, int max_con)
  {
    const int name_len = std::strlen(name);
    const int size = name_len + system_pipe_path.size();

    std::string full_name;

    full_name.reserve(size);
    full_name = system_pipe_path.data();
    full_name.append(name, name_len);

    debug_printerr("Opening pipe name %s (size:%d)\n",
                    full_name.c_str(),
                    full_name.size());

    return { host_create(full_name, max_con) };
  }

  fd_native_t
  listener_t::host_create(const std::string &name, unsigned max_con)
  {
    struct sockaddr_un addr;
    int listen_fd = socket(AF_UNIX, SOCK_STREAM, 0);

    addr.sun_family = AF_UNIX;
    strcpy(addr.sun_path, name.data());

    int len = name.size() + sizeof(addr.sun_family);

    mipc::host_unlink(name.c_str());

    if (UNLIKELY(bind(listen_fd,
                      reinterpret_cast<struct sockaddr *>(&addr),
                      len) == -1)) {
      debug_printerr("Failed to bind, with error %d\n", errno);
      return INVALID_FD;
    }

    listen(listen_fd, max_con);

    return listen_fd;
  }

  fd_native_t
  listener_t::host_accept()
  {
    fd_native_t conn = ::accept(this->fd, NULL, NULL);

    return conn;
  }

#endif

} // namespace mipc::pipe
