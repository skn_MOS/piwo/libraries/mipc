#include "shmem_native.h"
#include "debug_print.h"
#include "error.h"
#include "utils.h"

#include <cassert>

#ifdef __WIN32
#include <cinttypes>

#elif __linux__

#include <sys/mman.h>
#include <sys/stat.h>
#include <unistd.h>

#endif

mipc::shmem::shmem_native_t::shmem_native_t(
  shmem_native_t &&other) noexcept
  : fd(other.fd)
{
  other.fd = INVALID_FD;
}

void
mipc::shmem::shmem_native_t::operator=(shmem_native_t &&other) noexcept
{
  if (UNLIKELY(this == &other))
    return;

  this->close();

  this->fd = other.fd;

  other.fd = INVALID_FD;
}

mipc::shmem::shmem_native_t::~shmem_native_t() noexcept
{
  debug_printerr("Checking if current shmem file descriptor is valid.\n");

  if (!this->is_valid()) {
    debug_printerr("Shmem file decriptor not valid. Doing nothing.\n");
    return;
  }

  debug_printerr("Shmem file descriptor is valid, closing.\n");
  if (host_close(this->fd) == -1) {
    debug_printerr(
      "Failed to close file descriptor! Possible use after close? "
      "Another object owned the same file descriptor?\n");
  } else {
    debug_printerr("Shmem file descriptor successfully closed.\n");
  }
}

mipc::shmem::shmem_native_t
mipc::shmem::shmem_native_t::create(const char *name,
                                    size_t size,
                                    perm_t mode,
                                    perm_t access) noexcept
{
  return { host_create(name, size, mode, access) };
}

mipc::shmem::shmem_native_t
mipc::shmem::shmem_native_t::open(const char *name, perm_t mode) noexcept
{
  return host_open(name, mode);
}

int
mipc::shmem::shmem_native_t::close() noexcept
{
  auto ret = 0;

  if (this->is_valid()) {
    ret = this->host_close();
  }

  return ret;
}

int
mipc::shmem::shmem_native_t::_close() noexcept
{
  return this->host_close();
}

/* PLATFORM DEPENDENT IMPLEMENTATION */

/* WINDOWS IMPLEMENTAION */

#ifdef __WIN32

mipc::fd_native_t
mipc::shmem::shmem_native_t::host_create(const char *name,
                                         size_t size,
                                         [[maybe_unused]] perm_t mode,
                                         perm_t access) noexcept
{
  std::string full_name = "Local\\";
  full_name += name;

  fd_native_t fd = CreateFileMapping(INVALID_HANDLE_VALUE,
                                     NULL,
                                     perm2pageprot(access),
                                     0,
                                     size,
                                     full_name.c_str());

  // CAUTION ! BEFORE YOU CHANGE, READ BELOW !
  // Why api for shared memory returns NULL instead of INVALID_HANDLE_VALUE
  // is beyond me. So INVALID_FD cannot be used here.
  if (fd == NULL) {
    debug_printerr("CreateFileMapping failed name: %s, error: " PRId64 "\n",
                   full_name.c_str(),
                   GetLastError());
    return INVALID_FD;
  }

  return fd;
}

mipc::fd_native_t
mipc::shmem::shmem_native_t::host_open(const char *name,
                                       perm_t perm) noexcept
{
  std::string full_name = "Local\\";
  full_name += name;

  fd_native_t fd =
    OpenFileMapping(perm2mapacc(perm), FALSE, full_name.c_str());

  if (fd == NULL) {
    debug_printerr(
      "Shared memory opening failed (%s) with code " PRId64 "!\n",
      full_name.c_str(),
      GetLastError());

    return INVALID_FD;
  }

  return fd;
}

int
mipc::shmem::shmem_native_t::host_close() noexcept
{
  return CloseHandle(this->fd);
}

int
mipc::shmem::shmem_native_t::host_close(mipc::fd_native_t fd) noexcept
{
  return CloseHandle(fd);
}

int
mipc::shmem::shmem_native_t::host_remove([
  [maybe_unused]] const char *name) noexcept
{
  // Windows removes shared memory by itself when all handles are closed.
  return 0;
}

/* LINUX IMPLEMENTATION */

#elif __linux__

mipc::fd_native_t
mipc::shmem::shmem_native_t::host_create(const char *name,
                                         size_t size,
                                         perm_t mode,
                                         perm_t access) noexcept
{
  // TODO unlinking by default might not be the best idea
  host_remove(name);

  fd_native_t fd = shm_open(
    name, perm2flags(mode) | O_CREAT | O_EXCL, perm2access(access));

  if (fd == INVALID_FD) {

    debug_printerr(
      "Shared memory creation failed (%s) with code %d!\n",
      name,
      errno);

    return fd;
  }

  if (!!ftruncate(fd, size)) {

    debug_printerr("Resizeing shared memory %s failed! "
                   "System error code: %d\n",
                   name,
                   errno);

    host_close(fd);
    host_remove(name);

    return INVALID_FD;
  }

  return fd;
}

mipc::fd_native_t
mipc::shmem::shmem_native_t::host_open(const char *name,
                                       perm_t mode) noexcept
{
  fd_native_t fd = ::shm_open(name, O_RDWR, utils::underlay_cast(mode));

  if (fd < 0) {

    debug_printerr(
      "Failed to open shared memory (name: %s, errc: %d)\n", name, errno);

    return INVALID_FD;
  }

  return fd;
}

int
mipc::shmem::shmem_native_t::host_close() noexcept
{
  return ::close(this->fd);
}

int
mipc::shmem::shmem_native_t::host_remove(const char *name) noexcept
{
  return ::shm_unlink(name);
}

int
mipc::shmem::shmem_native_t::host_close(fd_native_t fd) noexcept
{
  return ::close(fd);
}

#endif
