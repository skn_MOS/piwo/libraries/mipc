#include "file_native.h"

/* PLATFORM DEPENDEND DEFINITIONS */

namespace mipc {

#ifdef __WIN32

  fd_native_t
  host_open(const char *file_name, perm_t mode) noexcept
  {
    return CreateFile(file_name,
                      perm2generic(mode),
                      0,
                      NULL,
                      OPEN_EXISTING,
                      FILE_ATTRIBUTE_NORMAL,
                      NULL);
  }

  fd_native_t
  host_create(const char *file_name, perm_t mode, perm_t access) noexcept
  {
    return CreateFile(file_name,
                      perm2generic(mode),
                      perm2sharemode(access),
                      NULL,
                      CREATE_NEW,
                      FILE_ATTRIBUTE_NORMAL,
                      NULL);
  }

  void
  host_close(fd_native_t fd) noexcept
  {
    CloseHandle(fd);
  }

  void
  host_unlink(const char *file_name) noexcept
  {
    DeleteFile(file_name);
  }

  size_t
  host_filesize(fd_native_t fd) noexcept
  {
    DWORD size_high;
    DWORD size_low = GetFileSize(fd, &size_high);

    if (size_low == INVALID_FILE_SIZE)
      return 0;

    if constexpr (sizeof(size_t) == 2 * sizeof(DWORD))
      return (static_cast<size_t>(size_high) << 32) |
             static_cast<size_t>(size_low);
    else
      return size_low;
  }

#elif __linux__

  fd_native_t
  host_open(const char *file_name, perm_t perm) noexcept
  {
    return ::open(file_name, perm2flags(perm));
  }

  fd_native_t
  host_create(const char *file_name, perm_t mode, perm_t access) noexcept
  {
    return ::open(
      file_name, perm2flags(mode) | O_CREAT | O_EXCL, perm2access(access));
  }

  void
  host_close(fd_native_t fd) noexcept
  {
    ::close(fd);
  }

  void
  host_unlink(const char *file_name) noexcept
  {
    ::unlink(file_name);
  }

  size_t
  host_filesize(fd_native_t fd) noexcept
  {
    struct stat finfo;

    if (fstat(fd, &finfo) == -1)
      return 0;

    return static_cast<size_t>(finfo.st_size);
  }

#endif

} // namespace mipc
