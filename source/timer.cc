#include <chrono>
#include <cmath>
#include <cstdio>
#include <cstring>
#include <functional>
#include <timer.h>

#include "timer_base.h"
#include "panic.h"

namespace mipc
{
timer::timer()
  : active(false)
{
#ifdef __WIN32
  hTimerQueue = CreateTimerQueue();
  if (NULL == hTimerQueue)
  {
    panic();
    return;
  }

  callback_events[utils::underlay_cast(event_id::TIMER)] =
    CreateEvent(NULL, TRUE, FALSE, NULL);
  if (NULL == callback_events[utils::underlay_cast(event_id::TIMER)])
  {
    DeleteTimerQueueEx(hTimerQueue, NULL);
    hTimerQueue = INVALID_TIMER_ID;

    panic();
    return;
  }

  callback_events[utils::underlay_cast(event_id::RESET)] =
    CreateEvent(NULL, TRUE, FALSE, NULL);
  if (NULL == callback_events[utils::underlay_cast(event_id::RESET)])
  {
    DeleteTimerQueueEx(hTimerQueue, NULL);
    CloseHandle(callback_events[utils::underlay_cast(event_id::TIMER)]);
    hTimerQueue                                            = INVALID_TIMER_ID;
    callback_events[utils::underlay_cast(event_id::TIMER)] = NULL;

    panic();
    return;
  }
#elif __linux__
  pfd[utils::underlay_cast(event_id::TIMER)].fd =
    timerfd_create(CLOCK_REALTIME, TFD_NONBLOCK);

  if (pfd[utils::underlay_cast(event_id::TIMER)].fd == -1)
  {
    pfd[utils::underlay_cast(event_id::TIMER)].fd = INVALID_TIMER_ID;

    panic();
    return;
  }
  pfd[utils::underlay_cast(event_id::TIMER)].events = POLLIN;

  pfd[utils::underlay_cast(event_id::RESET)].fd = eventfd(0, 0);
  if (pfd[utils::underlay_cast(event_id::RESET)].fd == -1)
  {
    close(pfd[utils::underlay_cast(event_id::TIMER)].fd);

    pfd[utils::underlay_cast(event_id::TIMER)].fd = INVALID_TIMER_ID;
    pfd[utils::underlay_cast(event_id::RESET)].fd = INVALID_TIMER_ID;

    panic();
    return;
  }
  pfd[utils::underlay_cast(event_id::RESET)].events = POLLIN;
#endif
}

timer::~timer()
{
  if (!stop() && is_active())
  {
    fprintf(stderr, "Error occured during stopping the timer\n");
  }

#ifdef __WIN32
  if (hTimerQueue)
    DeleteTimerQueueEx(hTimerQueue, NULL);
  hTimerQueue = NULL;

  if (callback_events[utils::underlay_cast(event_id::TIMER)])
    CloseHandle(callback_events[utils::underlay_cast(event_id::TIMER)]);

  if (callback_events[utils::underlay_cast(event_id::RESET)])
    CloseHandle(callback_events[utils::underlay_cast(event_id::RESET)]);

#elif __linux__
  close(pfd[utils::underlay_cast(event_id::TIMER)].fd);
  pfd[utils::underlay_cast(event_id::TIMER)].fd = INVALID_TIMER_ID;

  close(pfd[utils::underlay_cast(event_id::RESET)].fd);
  pfd[utils::underlay_cast(event_id::RESET)].fd = INVALID_TIMER_ID;
#endif
}

bool
timer::trigger_timer(std::chrono::seconds      sec,
                     std::chrono::milliseconds milisec,
                     bool                      single_shot)
{
#ifdef __WIN32
  auto flags = !single_shot ? WT_EXECUTEDEFAULT : WT_EXECUTEONLYONCE;

  auto interval =
    std::chrono::duration_cast<std::chrono::milliseconds>(sec).count() +
    milisec.count();

  auto duetime = single_shot ? interval : 0;
  auto period  = !single_shot ? interval : 0;

  if (interval == 0)
  {
    if (!hTimer) // already stopped
      return true;

    auto ret = DeleteTimerQueueTimer(hTimerQueue, hTimer, INVALID_HANDLE_VALUE);
    hTimer   = NULL;

    return ret != 0;
  }

  if (hTimer != NULL)
  {
    if (DeleteTimerQueueTimer(hTimerQueue, hTimer, INVALID_HANDLE_VALUE) != 0)
    {
      return false;
    }
  }

  if (!CreateTimerQueueTimer(&hTimer,
                             hTimerQueue,
                             (WAITORTIMERCALLBACK)mipc::timer_callback,
                             this,
                             duetime,
                             period,
                             flags))
  {
    return false;
  }

  return true;
#elif __linux__

  struct itimerspec its;

  its.it_value.tv_sec = sec.count();
  its.it_value.tv_nsec =
    std::chrono::duration_cast<std::chrono::nanoseconds>(milisec).count();

  if (!single_shot)
  {
    its.it_interval.tv_sec  = its.it_value.tv_sec;
    its.it_interval.tv_nsec = its.it_value.tv_nsec;
  }
  else
  {
    its.it_interval.tv_sec  = 0;
    its.it_interval.tv_nsec = 0;
  }

  if (timerfd_settime(pfd[utils::underlay_cast(event_id::TIMER)].fd,
                      TFD_TIMER_ABSTIME,
                      &its,
                      NULL) == -1)
  {
    return false;
  }

  return true;
#endif
}

bool
timer::wait()
{
#ifdef __WIN32
  auto dwEvent =
    WaitForMultipleObjects(EVENTS_CNT, callback_events, FALSE, INFINITE);

  if (dwEvent == WAIT_OBJECT_0 + utils::underlay_cast(event_id::TIMER))
  {
    ResetEvent(callback_events[utils::underlay_cast(event_id::TIMER)]);
    return true;
  }

  if (dwEvent == WAIT_OBJECT_0 + utils::underlay_cast(event_id::RESET))
  {
    ResetEvent(callback_events[utils::underlay_cast(event_id::RESET)]);
    return false;
  }

  return false;
#elif __linux__
  if (poll(pfd, EVENTS_CNT, POLLINF) < 0)
    return false;

  event_val_t read_value;
  bool ret = false;

  if (pfd[utils::underlay_cast(event_id::TIMER)].revents & POLLIN)
  {
    if (read(pfd[utils::underlay_cast(event_id::TIMER)].fd,
             &read_value,
             sizeof(read_value)) < 0)
    {
      fprintf(stderr, "Error occured durring reading the timer state.\n");
      ret = false;
    }
    else
    {
      ret = true;
    }
  }

  if (pfd[utils::underlay_cast(event_id::RESET)].revents & POLLIN)
  {
    if (read(pfd[utils::underlay_cast(event_id::RESET)].fd,
             &read_value,
             sizeof(read_value)) < 0)
    {
      fprintf(stderr, "error occured durring reading the reset state\n");
    }

    return false;
  }

  return ret;
#endif
}

bool
timer::start(std::chrono::seconds      sec,
             std::chrono::milliseconds milisec,
             bool                      single_shot)
{
  if (!is_valid())
    return false;

  const bool ret = trigger_timer(sec, milisec, single_shot);
  active         = ret;

  return ret;
}

bool
timer::stop()
{
  if (!is_valid() || !is_active())
    return false;

#ifdef __WIN32
  if (SetEvent(callback_events[utils::underlay_cast(event_id::RESET)]) == 0)
    return false;
#elif __linux__
  event_val_t trash = 0xff;
  if (write(pfd[utils::underlay_cast(event_id::RESET)].fd,
            &trash,
            sizeof(trash)) < 0)
  {
    return false;
  }
#endif

  const bool ret =
    trigger_timer(std::chrono::seconds{ 0 }, std::chrono::milliseconds{ 0 });

  active = !ret;

  return ret;
}

#ifdef __WIN32
void
timer_callback(PVOID lpParam, [[maybe_unused]] BOOLEAN TimerOrWaitFired)
{
  timer* t = reinterpret_cast<timer*>(lpParam);
  SetEvent(t->callback_events[utils::underlay_cast(timer::event_id::TIMER)]);
}
#endif
} // namespace mipc
