#include "file_guard.h"
#include "debug_print.h"
#include "file_native.h"

void
mipc::file_guard::unlink() noexcept
{
  if (this->name.empty())
    return;

  mipc::host_unlink(this->name.c_str());
}

mipc::file_guard::file_guard(file_guard &&fg) noexcept
  : name(std::move(fg.name))
{
  debug_printerr("%s\n", this->name.c_str());
}

void
mipc::file_guard::operator=(file_guard &&fg) noexcept
{
  debug_printerr("%s %s\n", this->name.c_str(), fg.name.c_str());

  // TODO this if is so wrong
  if (this->name != fg.name)
    this->unlink();

  this->name = std::move(fg.name);
  fg.name = "";
}

mipc::file_guard::~file_guard() noexcept
{
  debug_printerr("Unlinking file %s\n", this->name.c_str());
  this->unlink();
}
