#include "pipe_conn.h"
#include "debug_print.h"
#include "utils.h"

#include <cstring>
#include <string>

#ifdef __WIN32

#elif __linux__

#include <poll.h>
#endif

namespace mipc::pipe {

#ifdef __WIN32
  conn_t::conn_t(conn_t &&conn)
    : fd(conn.fd)
    , com_port(conn.com_port)
    , io_pending(conn.io_pending)
    , overlapped_info(conn.overlapped_info)

  {
    debug_printerr("Moving conn\n");
    ::CancelIo(conn.fd);
    conn.fd = INVALID_FD;
  }
#elif __linux__
  conn_t::conn_t(conn_t &&conn)
    : fd(conn.fd)
  {
    debug_printerr("Moving conn\n");
    conn.fd = INVALID_FD;
  }
#endif

  void
  conn_t::operator=(conn_t &&conn)
  {
    this->close();

    this->fd = conn.fd;
    conn.fd = INVALID_FD;

#ifdef __WIN32
    this->com_port = conn.com_port;
#endif
  }

  conn_t::~conn_t() noexcept { this->close(); }

  conn_t
  conn_t::open(const char *name)
  {
    const int name_len = std::strlen(name);
    const int size = name_len + system_pipe_path.size();

    std::string full_name;

    full_name.reserve(size);
    full_name = system_pipe_path.data();
    full_name.append(name, name_len);

    debug_printerr(
      "Opening pipe %s (size:%d)\n",
      full_name.c_str(),
      full_name.size());

    return { host_open(full_name.c_str()) };
  }

  void
  conn_t::close() noexcept
  {
    debug_printerr("Checking if fd valid\n");

    if (!mipc::fd_valid(this->fd)) {
      debug_printerr("Not valid, doing nothing.\n");
      return;
    }

    debug_printerr("Valid, closing.\n");

#ifdef __WIN32
    ::CancelIo(this->fd);
    mipc::host_close(this->com_port);
#endif

    mipc::host_close(this->fd);
    this->fd = INVALID_FD;
  }

#ifdef __WIN32

  fd_native_t
  conn_t::host_open(const char *name)
  {
    return CreateFile(name,
                      GENERIC_READ | GENERIC_WRITE,
                      FILE_SHARE_READ | FILE_SHARE_WRITE,
                      NULL,
                      OPEN_EXISTING,
                      FILE_ATTRIBUTE_NORMAL | FILE_FLAG_OVERLAPPED,
                      NULL);
  }

  int
  conn_t::write(const char *data, size_t size)
  {
    DWORD read;
    if (UNLIKELY(!::WriteFile(this->fd, data, size, &read, NULL)))
      return -1;

    return read;
  }

  int
  conn_t::read(char *buf, size_t size)
  {
    debug_printerr("Entering %s\n", __FUNCTION__);
    debug_printerr("Reading %zu into %p\n", size, (void *)buf);

    DWORD read;

    bool status = ::ReadFile(this->fd, buf, size, &read, NULL);
    this->io_pending = false;

    debug_printerr("Read %lu with status %d\n", read, status);
    if (status)
      return read;

    return -1;
  }

  // TODO(holz) this implementation can be done better for sure
  int
  conn_t::readn(char *buf, size_t n)
  {
    constexpr int def_timeout = 1000;
    int read = 0;
    while (n > 0) {

      auto wait_status = this->wait(def_timeout);

      if (wait_status < 0)
        return n;

      if (wait_status == 0)
        continue;

      int read_tmp = this->read(buf, n);

      // TODO(holz) redundant check?
      if (read_tmp < 0)
        return n;

      read += read_tmp;
      buf += read_tmp;
      n -= static_cast<size_t>(read_tmp);
    }

    return read;
  }

  bool
  conn_t::enqueue_read() noexcept
  {
    if (this->io_pending) {
      // This actually means NO error.
      // Caller should expect return false since it is async read by
      // design.
      return false;
    }

    [[maybe_unused]] DWORD read;
    this->io_pending = true;
    this->overlapped_info = {};
    return ReadFile(this->fd, NULL, 0, &read, &this->overlapped_info);
  }

  int
  conn_t::wait(int timeout) noexcept
  {
    debug_printerr("Entering %s\n", __FUNCTION__);

    LPOVERLAPPED overlaped_ptr = nullptr;
    DWORD to_read;
    ULONG_PTR key = 1;

    // Will always return error, but GetLastError should return
    // ERROR_IO_PENDING.
    bool status = this->enqueue_read();

    status = GetQueuedCompletionStatus(
      this->com_port, &to_read, &key, &overlaped_ptr, timeout);
    printf("To read: %lu\n", to_read);

    if (overlaped_ptr == nullptr) {

      auto herr = host_error();
      debug_printerr("GetCompletionStatus failed with error %d\n", herr);

      if (herr == WAIT_TIMEOUT)
        return 0;

      return -1;
    }

    return 1;
  }

#elif __linux__

  fd_native_t
  conn_t::host_open(const char *name)
  {
    struct sockaddr_un addr;
    fd_native_t fd = socket(AF_UNIX, SOCK_STREAM | SOCK_NONBLOCK, 0);

    addr.sun_family = AF_UNIX;
    strcpy(addr.sun_path, name);

    int len = strlen(name) + sizeof(addr.sun_family);

    if (::connect(fd, (struct sockaddr *)&addr, len) == -1) {
      ::close(fd);
      return INVALID_FD;
    }

    return fd;
  }

  int
  conn_t::write(const char *data, size_t size)
  {
    return ::send(this->fd, data, size, MSG_NOSIGNAL);
  }

  int
  conn_t::read(char *buf, size_t size)
  {
    return ::read(this->fd, buf, size);
  }

  int
  conn_t::readn(char *buf, size_t n)
  {
    auto ret = recv(this->fd, buf, n, MSG_WAITALL);

    if (UNLIKELY(ret < 0))
      return n;

    return n - ret;
  }

  int
  conn_t::wait(int timeout) noexcept
  {
    constexpr int fd_count = 1;

    struct pollfd info;
    info.fd = this->fd;
    info.events = POLLIN;

    int status = poll(&info, fd_count, timeout);

    // Pipe closed.
    if (info.revents & POLLHUP)
      status = -1;

    return status;
  }

#endif

} // namespace mipc::pipe
