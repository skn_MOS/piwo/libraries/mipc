#include <chrono>
#include <mipc/timer.h>
#include <functional>

void handler_async()
{
    fprintf(stderr, "Timer async work\n");
}

bool run_timer(mipc::timer_async& t, int sec, int mili, int sleep_time)
{
  printf("Timer settings:\n");
  printf("\tInterval seconds: %u\n", sec);
  printf("\tInterval miliseconds: %u\n", mili);
  if(t.run(std::chrono::seconds(sec), 
              std::chrono::milliseconds(mili)) == false)
  {
      fprintf(stderr, "Running the asyn timer failed\n");
      fprintf(stderr, "Reason %u\n", t.host_error());
      return false;
  }

  int sleep_left = sleep_time;
#ifdef __WIN32
#define SEC_TO_MILISEC 1000
  Sleep(sleep_time*SEC_TO_MILISEC);
#elif __linux__
  do {
    sleep_left = sleep(sleep_left);
  } while (sleep_left);
#endif

  t.join();

  return true;
}

int main()
{
    auto t_async = mipc::timer_async (handler_async);
    printf("Run timer first time\n");
    run_timer(t_async, 1, 0, 3);
    // reuse the same timer second time
    printf("Run timer second time\n");
    run_timer(t_async, 0, 20, 1);
}
