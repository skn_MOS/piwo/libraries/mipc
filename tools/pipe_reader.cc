#include <cstdint>
#include <cstdio>
#include <cstdlib>
#include <mipc/base.h>
#include <mipc/pipe_conn.h>
#include <string>
#include <vector>

void
usage(const char *program_name)
{
  printf("Usage: %s <pipe_name> <buffer_size> <timeout(milliseconds)>\n",
         program_name);
}

void
discard_line()
{
  while (getchar() != '\n') {
  }
}

int
main(int argc, char **argv)
{
  using namespace mipc::pipe;

  if (argc != 4) {
    usage(argv[0]);
    return 1;
  }

  mipc::init();

  auto pipe = mipc::pipe::conn_t::open(argv[1]);

  if (!pipe.is_valid()) {
    printf("Failed to open pipe. %u\n", mipc::pipe::conn_t::host_error());
    return 1;
  }

  puts("Opened");
  auto buf_size = atoll(argv[2]);

  if (buf_size <= 0) {
    printf("buffer_size parameter must be > 0.");
    usage(argv[0]);
    return 1;
  }

  auto timeout = atoll(argv[3]);

  if (timeout <= 0) {
    printf("timeout parameter must be > 0.");
    usage(argv[0]);
    return 1;
  }

  printf("Pipe opened.\nReading in packs of %u bytes.\n", buf_size);
  std::vector<char> buf;
  buf.resize(buf_size);

  while (1) {
    auto wait_status = pipe.wait(timeout);

    if (wait_status == -1) {
      puts("Error");
      break;
    }

    if (wait_status == 0) {
      puts("Timed out");
      continue;
    }

    auto ret = pipe.read(buf.data(), static_cast<size_t>(buf_size));

    fprintf(stderr, "Ret: %d\n", ret);
    if (ret == -1 || ret == 0) {
      fprintf(stderr, "Errc: %u", mipc::pipe::conn_t::host_error());
      return 0;
    }

    if(ret < buf_size)
      buf.data()[ret] = '\0';

    fprintf(stderr, "%s\n", buf.data());
  }
}
