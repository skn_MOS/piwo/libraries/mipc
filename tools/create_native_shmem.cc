#include <mipc/base.h>
#include <mipc/file_native.h>
#include <mipc/shmem_native.h>
#include <stdio.h>
#include <string>

using namespace mipc::shmem;
using mipc::perm_t;

void
usage(const char *program_name)
{
  puts("MIPC -- create shared memory");
  printf("Usage: %s [shm_name shmem_size mode access]\n", program_name);
  puts("shmem_name -- name of shared memory (will be automatically "
       "prefixed with mos_shmem_)");
  puts("shmem_size -- size of shared memory in bytes, cannot be zero");
  puts("mode       -- open mode for shared memory can be R, W or RW "
       "(default: R)");
  puts("access     -- access mode for shared memory can be R, W or RW "
       "(default: R)");
}

perm_t
parse_mode_from_string(std::string_view mode)
{
  if (mode == "R")
    return perm_t::RDONLY;

  if (mode == "W")
    return perm_t::WRONLY;

  if (mode == "RW")
    return perm_t::RDWR;

  return perm_t::RDONLY;
}

int
main(int argc, char **argv)
{
  constexpr const char *shmem_def_name = "shmemtest";
  constexpr size_t shmem_def_size = 512;
  constexpr perm_t shmem_def_mode = perm_t::RDWR;

  std::string shmem_name = "mos_shmem_";
  size_t shmem_size;
  perm_t mode;
  perm_t access;

  if (argc != 1 && argc != 5) {
    usage(argv[0]);
    return 1;
  }

  mipc::init();

  if (argc == 1) {
    shmem_name += shmem_def_name;
    shmem_size = shmem_def_size;
    mode = shmem_def_mode;
  } else {
    shmem_name += argv[1];
    shmem_size = std::stoi(argv[2]);
    mode = parse_mode_from_string(std::string_view(argv[3]));
    access = parse_mode_from_string(std::string_view(argv[4]));
  }

  if (shmem_size == 0) {
    puts("Invalid shmem_size!");
    usage(argv[0]);
  }

  auto shmem =
    shmem_native_t::create(shmem_name.c_str(), shmem_size, mode, access);

  if (!shmem.is_valid()) {
    puts("Shared memory creation failed!");
    return 2;
  }

  puts("Shared memory created. Press any key to exit. . .");
  getchar();
}
