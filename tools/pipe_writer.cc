#include <cstdlib>
#include <iostream>
#include <string>
#include <string_view>

#include <mipc/base.h>
#include <mipc/pipe_listener.h>
#include <mipc/utils.h>

#include "pipe_writer.h"

using pipe_t = mipc::pipe::listener_t;

static void
usage(const char *program_name)
{
  printf(
    "Usage: %s <pipe_name> [listen] [hex,dec] [timeout=<timeout in ms>]\n",
    program_name);
}

static int
write_handler(int argc, char **argv)
{
  std::string line;

  trans_proc_t in_trans_proc;

  switch (options.tmode) {
    using tm = transform_mode_t;

    case tm::HEX:
      in_trans_proc = hex_trans;
      break;

    case tm::DEC:
      in_trans_proc = dec_trans;
      break;

    case tm::NONE:
    default:
      in_trans_proc = no_trans;
      break;
  }

  auto pipe = mipc::pipe::conn_t::open(argv[1]);

  if (!pipe.is_valid()) {
    puts("Opening pipe failed");
    return 1;
  }

  puts("Pipe opened.\nInputed data will be send via pipe after hitting "
       "enter.");
  std::string_view data_to_send(argv[2]);

  while (1) {
    std::cin >> line;
    line = in_trans_proc(line);

    // TODO perhaps should check if all the data transfered
    // now its only handling complete success or failure
    if (pipe.write(line.data(), line.size()) == -1) {
      printf("Client closed connection\n");
      break;
    }
  }

  return 0;
}

static int
listen_handler(int argc, char **argv)
{
  std::string line;

  trans_proc_t in_trans_proc;

  switch (options.tmode) {
    using tm = transform_mode_t;

    case tm::HEX:
      in_trans_proc = hex_trans;
      break;

    case tm::DEC:
      in_trans_proc = dec_trans;
      break;

    case tm::NONE:
    default:
      in_trans_proc = no_trans;
      break;
  }

pipe_listen:
  printf("Listening for connection . . .\n");
  auto listener = pipe_t::create(argv[1]);

  if (!listener.is_valid()) {
    printf("Failed to open pipe listener.\n");
    return 1;
  }

  puts("Pipe created. Waiting for client.");
  auto pipe = listener.accept();

  puts("Pipe opened.\nInputed data will be send via pipe after hitting "
       "enter.");
  std::string_view data_to_send(argv[2]);

  while (1) {
    std::cin >> line;
    line = in_trans_proc(line);

    // TODO perhaps should check if all the data transfered
    // now its only handling complete success of failure
    if (pipe.write(line.data(), line.size()) == -1) {
      printf("Client closed connection\n");
      goto pipe_listen;
    }
  }

  return 0;
}

using timeout_t = decltype(options_::timeout);
constexpr timeout_t max_timeout = std::numeric_limits<timeout_t>::max();
constexpr auto def_timeout = max_timeout;
constexpr std::string_view timeout_opt = "timeout=";

static timeout_t
parse_timeout(const std::string &opt)
{
  auto valbegin = opt.c_str() + timeout_opt.size();
  return strtoull(valbegin, nullptr, 10);
}

static void
opt_parse(int argc, char **argv)
{
  for (auto i = 2; i < argc; ++i) {
    std::string arg = argv[i];
    printf("arg %d\n", arg);
    if (arg == "listen")
      options.listen = true;
    else if (arg == "hex")
      options.tmode = transform_mode_t::HEX;
    else if (arg == "dec")
      options.tmode = transform_mode_t::DEC;
    else if (mipc::utils::starts_with(arg, timeout_opt))
      options.timeout = parse_timeout(arg);
  }

  if (options.timeout == 0)
    options.timeout = def_timeout;
}

int
main(int argc, char **argv)
{
  if (argc < 3) {
    usage(argv[0]);
    return 1;
  }

  mipc::init();

  opt_parse(argc, argv);
  options.print();

  if (options.listen)
    return listen_handler(argc, argv);
  else
    return write_handler(argc, argv);
}
