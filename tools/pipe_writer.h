#pragma once
#include <string>

#include <mipc/utils.h>

enum class transform_mode_t
{
  NONE,
  HEX,
  DEC
};

static struct options_
{
  bool listen = false;
  transform_mode_t tmode = transform_mode_t::NONE;
  int timeout = 0;

  std::string
  tmode2str()
  {
    using tm = transform_mode_t;
    switch (tmode) {
      case tm::NONE:
        return "NONE";
      case tm::HEX:
        return "HEX";
      case tm::DEC:
        return "DEC";
      default:
        return "Unknown";
    }
  }

  void
  print()
  {
    printf("listen: %s, tmode %s, timeout %d\n",
               this->listen ? "true" : "false",
               this->tmode2str(),
               this->timeout);
  }
} options;

static std::string
in_trans(const std::string &in, int base)
{
  std::string ret;
  auto next_val = in.c_str();
  auto next_val_tmp = next_val;
  auto in_end = &(*in.end());

  while (next_val < in_end) {
    auto val =
      strtoull(next_val, const_cast<char **>(&next_val_tmp), base);

    if (UNLIKELY(next_val_tmp == next_val))
      break;

    next_val = next_val_tmp;

    if (UNLIKELY(val > 0xff))
      continue;

    ret.append(1, static_cast<char>(val));
  }

  return ret;
}

using trans_proc_t = std::string (*)(const std::string &);

static std::string
hex_trans(const std::string &in)
{
  return in_trans(in, 16);
}

static std::string
dec_trans(const std::string &in)
{
  return in_trans(in, 10);
}

static std::string
no_trans(const std::string &in)
{
  return in;
}
