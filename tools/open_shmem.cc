#include <cstdint>
#include <cstdio>
#include <mipc/base.h>
#include <mipc/shmem.h>

static void
usage(const char *program_name)
{
  printf("Usage: %s <shmem_name>\n", program_name);
}

template<typename IteratorType>
IteratorType
find_config_end(IteratorType begin, size_t mem_len)
{
  uint_fast32_t level = 0;
  IteratorType *end = begin + mem_len;

  do {
    char c = *begin;

    switch (c) {
      case '{':
        ++level;
        break;
      case '}':
        --level;
        break;
      default:
        break;
    }

    if (level == 0) {
      return begin;
    }

    ++begin;

  } while (begin < end);

  return begin;
}

int
main(int argc, char **argv)
{
  using namespace mipc::shmem;
  mipc::init();

  if (argc != 2) {
    usage(argv[0]);
    return 1;
  }

  shmem_t shmem = shmem_t::open(argv[1]);

  if (!shmem.is_valid()) {
    puts("Failed to open shmem.");
    return 1;
  }

  printf("Opened with name %s\n", argv[1]);

  auto [mem_begin, mem_size] = shmem.get_mmapping();
  printf("Memory mapped at: %p with size %lu and header len %lu\n",
         mem_begin,
         mem_size,
         shmem.get_header_size());

  puts("Contents of shmem:\n========");

  fwrite(mem_begin, sizeof(std::byte), mem_size, stdout);

  printf("\nPress any key to exit. . .");
  getchar();
}
