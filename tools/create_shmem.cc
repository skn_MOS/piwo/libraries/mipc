#include <algorithm>
#include <cstdio>
#include <mipc/base.h>
#include <mipc/shmem.h>

void
usage(const char *program_name)
{
  printf(
    "Usage: %s <shmem_name> <shmem_config_filename> <data_to_output>\n",
    program_name);
}

int
main(int argc, char **argv)
{
  using namespace mipc::shmem;

  mipc::init();

  if (argc != 4) {
    usage(argv[0]);
    return 1;
  }

  shmem_t shmem = shmem_t::create(argv[1], argv[2]);

  if (!shmem.is_valid()) {
    puts("Failed to create shm.");
    return 1;
  }

  printf("Created with name %s and config %s\n", argv[1], argv[2]);

  std::basic_string_view<std::byte> data_to_output(
    reinterpret_cast<std::byte *>(argv[3]));

  auto [shmem_addr, size] = shmem.get_mmapping();
  auto payload = shmem.get_payload();

  size_t trunc_size = std::min(data_to_output.size(), size);

  std::copy(data_to_output.cbegin(), data_to_output.cend(), payload);

  printf("\nPress any key to exit. . .");
  getchar();
}
