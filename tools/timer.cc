#include <chrono>
#include <functional>
#include <mipc/timer.h>

void
handler(mipc::timer &t)
{
  while (t.wait()) {
    fprintf(stderr, "Wait returned \n");
  }
}

bool
run_timer(mipc::timer &t, int sec, int mili, int sleep_time)
{
  printf("Timer settings:\n");
  printf("\tInterval seconds: %u\n", sec);
  printf("\tInterval miliseconds: %u\n", mili);
  if (!t.is_valid()) {
	  fprintf(
      stderr, "Creating timer failed, error occured %u\n", t.host_error());
    return false;
  }

  if (!t.start(std::chrono::seconds{ sec },
               std::chrono::milliseconds{ mili })) {
    fprintf(stderr, "Arming failed, error occured %u\n", t.host_error());
    return false;
  }

  auto th = std::thread(handler, std::ref(t));
  int sleep_left = sleep_time;
#ifdef __WIN32
#define SEC_TO_MILISEC 1000
  Sleep(sleep_time*SEC_TO_MILISEC);
#elif __linux__
  do {
    sleep_left = sleep(sleep_left);
  } while (sleep_left);
#endif

  if (t.stop() == false) {
    fprintf(
      stderr, "Disarming failed, error occured %u\n", t.host_error());
    th.join();
    return false;
  }
  th.join();
  return true;
}

int
main()
{
  mipc::timer t;
  printf("Run timer first time\n");
  run_timer(t, 1, 0, 3);
  // reuse the same timer second time
  printf("Run timer second time\n");
  run_timer(t, 0, 20, 1);
}
