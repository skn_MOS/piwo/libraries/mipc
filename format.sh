#!/bin/bash
find source -type f -regex ".*\.\(cpp\|cc\|hpp\|h\)" | xargs clang-format -i -style=file
