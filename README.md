# MIPC
### Library provides
  - shared memory
  - pipes
  - timers

### Library overview
MIPC provides crossplatform (windows and linux) API for handling inter process
communication. One of the main goals of mipc is consistent observable behavior
of its features across all platforms.  However some differences in behavior
unfortunetaly exists and user should be aware of it and handle it properly.
For further information see "Platform dependent behavior" section.

### Installation
Below you can find description for building and installing this project using
__GCC__ and __GNU Make__, however You can use whatever compiler and build
system you want, as long as cmake supports it, or you know what you are doing.

#### Windows

1. Install __MinGW g++__ compiler (must support c++17)
2. Install __GNU make__
3. Install __CMake__
4. Add __MinGW__'s bin folder to `PATH` variable (eg. C:\MinGW\bin)
5. Add __GNU make__'s path to `PATH` variable (eg. C:\Program Files\GnuWin32\bin)
6. Add __CMake__'s path to `PATH` variable (eg. C:\Program Files\CMake\bin)

#### Linux

1. Install __g++__ (g++-8 or newer is preferred, also must support c++17)
2. Install __make__
3. Install __CMake__

#### Dependencies
You will have to install __RapidJSON__ headers. You might want to choose to install it
by using your favorite package manager, however installing from source, using cmake
is strongly recommended [RapidJSON source](https://github.com/Tencent/rapidjson/).
See `Why installing RapidJSON from source is required?` for more details.

### Building

#### Windows

1. Open terminal (eg. cmd.exe)
2. Go to location of main directory of this repo
3. Create new directory called __build__ with `mkdir build`
4. Enter new directory with `cd build`
5. Type in `cmake -G "Unix Makefiles" ..`
6. Type in `make install` (You may need to run cmd.exe with Administrator priviliges)

#### Linux

1. Open terminal
2. Go to location of main directory of this repo
3. Create new directory called __build__ with `mkdir build`
4. Enter new directory with `cd build`
5. Type in `cmake ..`
6. Type in `make install` (You may need to run this step as root, for example via sudo)

### Features
Currently supported features are bidirectional fifo named pipes, shared
memory objects and timers. Shared memory is configured with json file and this json file is
written to the shared memory itself to provide its configuration to the client.
There are no requirements regarding contents of such configuration file - its up
to the user to decide what information he or she wants to put there.
Nonetheless writing the config file is performed automatically by the library as
well as reading and parsing and user can later read the values from the
assosiative container. The mipc delivers two kinds of timers, the normal timer and
async timer. The normal timer shares the API that allows creating timer, 
arming with specified time, disarming and waiting for timer expiration. 
When it comes to the async timer, we can pass the
function that will be invoked on a additional thread when timer expired.

#### Pipes nonblocking IO by default
Pipes uses nonblocking IO by default, if one wants to wait on IO, one should use
wait() function which allows user to also specify timeout.  Wait then returns
when data is available or timeout is reached.

#### Platform dependent behavior
Although majority of this library delivers identical observable behavior across
platforms some differences are hard to avoid completely.  Normally when shared
memory or pipe object is destroyed its dtor removes registered name from local
filesystem and this is true for all platforms.  However, since Windows handles
creation of such object outside of filesystem (it implements it in the kernel)
- it is responsible for cleanup when application is terminated.  But on linux
when program uses pipes or shared memory and is not properly terminated (dtor
of the object is not called), the name of the object is "leaked" ie. stays in
the filesystem and blocks creatiion of new objects with the same name.  Users
then should handle this unfortunate situtaion themselves, by manually removing
it or on the program restart cleanup filesystem (also manually) from such
files (atleast for now).
Also see `Recreate pipe listener after failure on Windows`.

### Known issues
#### Recreate pipe listener after failure on Windows
Windows implementation of named pipes requires recreation of named pipe server
side if the connected client disconnects. This might be patched in the future
so the accept function automaticaly reinitializes the pipe server on failure.
Linux implementation does not suffer from such inconvenience.

#### Why installing RapidJSON from source is required?
This is the only way to provide RapidJSOM-config.cmake file, which is needed
by cmake when configuring this project. Dependency is not marked `REQUIRED`.
However It strongly recommended to provide such file. Because It might break
build process in the future.
