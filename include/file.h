#pragma once

#include "utils.h"

#include <charconv>
#include <cstddef>

#include <limits>

namespace mipc::shmem {
  class mmapping_t;
}

namespace mipc {

  // DONT USE THIS MECHANISM UNLESS YOU KNOW EXACLY KNOW WHAT YOU ARE DOING
  struct finbuf_unsafe_init
  {
    explicit constexpr finbuf_unsafe_init(size_t size, const char *data)
      : size(size)
      , data(data)
    {}

    size_t size;
    const char *data;
  };

  // File input buffer.
  // This class provides buffer-like (array-like) view over file.
  // If file is small enough It allocates buffer and copies the file to
  // local memory, if file is big however It memory map It. Used variant is
  // completely transparent to the user.
  class finbuf
  {
  public:
    // Choosen randomly out of thin air!
    // TODO(holz): measure and pick better value
    constexpr static size_t MAX_BUFFER_SIZE = 4096 * 4;

    finbuf(const char *filename);

    finbuf(shmem::mmapping_t &&mmapping);

    finbuf(finbuf_unsafe_init init)
      : _size(init.size)
      , _data(init.data)
    {}

    finbuf &
    operator=(finbuf_unsafe_init init)
    {
      this->free_buffer();

      this->_size = init.size;
      this->_data = init.data;

      return *this;
    }

    finbuf(const finbuf &other) = delete;

    finbuf(finbuf &&other)
      : _size(other._size)
      , _data(other._data)
    {
      other._size = 0;
      other._data = nullptr;
    }

    finbuf &
    operator=(const finbuf &other) = delete;

    finbuf &
    operator=(finbuf &&other)
    {
      this->_size = other._size;
      this->_data = other._data;

      other._size = 0;
      other._data = nullptr;

      return *this;
    }

    finbuf() { this->def_init(); }

    auto
    begin() const noexcept FORCE_INLINE
    {
      return this->_data;
    }

    auto
    end() const noexcept FORCE_INLINE
    {
      return this->_data + this->_size;
    }

    auto
    size() const noexcept FORCE_INLINE
    {
      return this->_size;
    }

    auto
    data() const noexcept FORCE_INLINE
    {
      return this->_data;
    }

    operator bool() const noexcept { return this->_data != nullptr; }

    ~finbuf() { this->free_buffer(); }

  private:
    void
    free_buffer() const noexcept;

    void
    def_init() FORCE_INLINE
    {
      this->_size = 0;
      this->_data = nullptr;
    }

  private:
    size_t _size;
    const char *_data;
  };
} // namespace mipc
