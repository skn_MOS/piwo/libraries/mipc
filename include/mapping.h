#pragma once
#include "mapping_native.h"
#include "permission.h"
#include "shmem_native.h"

#include <cstddef>
#include <cstdint>
#include <tuple>

namespace mipc::shmem {

  class mmapping_t
  {

  public:
    static constexpr perm_t default_perm = perm_t::RDWR;

  private:
    std::byte *addr;
    std::size_t size;

    mmapping_t(std::byte *addr, std::size_t size) noexcept;

  public:
    mmapping_t() noexcept;
    mmapping_t(const mmapping_t &other) = delete;
    mmapping_t(mmapping_t &&other) noexcept;

    void
    operator=(const mmapping_t &rhs) = delete;

    void
    operator=(mmapping_t &&rhs) noexcept;

    ~mmapping_t() noexcept;

    // Opens file, mapps it into memory and returns mapped mem
    static mmapping_t
    map(const char *file_name, perm_t mode = default_perm) noexcept;

    static mmapping_t
    map(fd_native_t shmem_fd,
        size_t size,
        perm_t mode = default_perm) noexcept;

    void
    unmap() noexcept;

    decltype(auto)
    get_addr() const noexcept
    {
      return this->addr;
    }

    decltype(auto)
    get_size() const noexcept
    {
      return this->size;
    }

    decltype(auto)
    begin() const noexcept
    {
      return this->get_addr();
    }

    decltype(auto)
    end() const noexcept
    {
      return this->begin() + this->size;
    }

    bool
    is_valid() const noexcept
    {
      return this->addr != INVALID_MAP;
    }

    constexpr std::tuple<std::byte *, std::size_t>
    as_tuple() const noexcept
    {
      return { this->addr, this->size };
    }

    void
    release_() noexcept
    {
      this->addr = INVALID_MAP;
    }
  };

} // namespace mipc::shmem
