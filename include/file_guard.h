#pragma once
#include <string>
#include <utility>

namespace mipc {

  class file_guard
  {
  private:
    std::string name;

  public:
    template<typename StringType>
    file_guard(StringType &&s)
      : name(std::forward<StringType>(s))
    {}

    file_guard() = default;
    file_guard(const file_guard &fg) = delete;
    file_guard(file_guard &&fg) noexcept;

    void
    operator=(const file_guard &fg) = delete;
    void
    operator=(file_guard &&fg) noexcept;

    ~file_guard() noexcept;

    void
    unlink() noexcept;
  };

} // namespace mipc
