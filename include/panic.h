#pragma once
#include "debug_print.h"

#if CONFIG_FATAL_PANIC
  #define panic()                                              \
    {                                                          \
      log_stderr("This should never happen!");                 \
      exit(1);                                                 \
    }
#else
  #define panic()                                              \
    {                                                          \
     log_stderr("This should never happen!");                  \
    }
#endif
