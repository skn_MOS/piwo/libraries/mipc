#pragma once

#include "file_native.h"
#include "permission.h"
#include "utils.h"

#include <unistd.h>

namespace mipc {

  ssize_t
  host_read(fd_native_t fd, char *buf, ssize_t count)
  {
    if (UNLIKELY(count < 0))
      return -1;

    return ::read(fd, buf, static_cast<size_t>(count));
  }

  ssize_t
  host_read_all(fd_native_t fd, void *buf_v, ssize_t scount)
  {
    if (UNLIKELY(scount < 0))
      return -1;

    char *buf = reinterpret_cast<char *>(buf_v);
    size_t count = static_cast<size_t>(scount);
    size_t bytes_read = 0;

    while (bytes_read < count) {
      ssize_t bytes_read_tmp = host_read(fd, buf, count - bytes_read);

      if (bytes_read_tmp < 0)
        return -1;

      // EOF
      if (bytes_read_tmp == 0)
        break;

      bytes_read += static_cast<size_t>(bytes_read_tmp);
      buf += static_cast<size_t>(bytes_read_tmp);
    }

    return bytes_read;
  }

} // namespace mipc
