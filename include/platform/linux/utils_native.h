#pragma once

#include <cerrno>
#include <cstdint>

namespace mipc::utils {

  inline int64_t
  get_last_system_error()
  {
    return errno;
  }

} // namespace mipc::utils
