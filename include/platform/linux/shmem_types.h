#pragma once
#include "file_guard.h"

namespace mipc {
  // Identifier for shared object (file) on linux is
  // name of file. Its used when unlinking on shmem destroy.
  typedef file_guard shobj_id_t;
} // namespace mipc
