#pragma once

#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include "file_native.h"

// C++ doesnt allow reinterpret cast in context of constexpr,
// so this has to be handled with C style define.
#define INVALID_MAP ((std::byte *)(-1))

namespace mipc {
  inline void *
  host_map(fd_native_t fd, size_t size, perm_t prot) noexcept
  {
    return ::mmap(NULL, size, perm2prot(prot), MAP_SHARED, fd, 0);
  }

  inline void
  host_unmap(void *addr, size_t size) noexcept
  {
    ::munmap(addr, size);
  }
} // namespace mipc
