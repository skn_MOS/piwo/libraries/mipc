#pragma once
#include <time.h>
#include <signal.h>
#include <errno.h>
#include <poll.h>
#include <sys/eventfd.h>
#include <sys/timerfd.h>
#include <unistd.h>

namespace mipc {

constexpr int INVALID_TIMER_ID = -1;

}
