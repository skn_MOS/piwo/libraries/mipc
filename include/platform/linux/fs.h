#pragma once
#include "permission.h"

#include <fcntl.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#define INVALID_FD -1

namespace mipc {

  typedef int fd_native_t;

  constexpr int
  perm2flags(perm_t perm) noexcept
  {
    switch (perm) {
      case perm_t::RDONLY:
        return O_RDONLY;
      case perm_t::WRONLY:
        return O_WRONLY;
      case perm_t::RDWR:
        return O_RDWR;
      default:
        return O_RDONLY;
    }
  }

  constexpr int
  perm2access(perm_t perm) noexcept
  {
    constexpr int p_rdonly = S_IRUSR | S_IRGRP;
    constexpr int p_wronly = S_IWUSR | S_IWGRP;
    constexpr int p_rdwr = p_rdonly | p_wronly;

    switch (perm) {
      case perm_t::RDONLY:
        return p_rdonly;
      case perm_t::WRONLY:
        return p_wronly;
      case perm_t::RDWR:
        return p_rdwr;
      default:
        return p_rdonly;
    }
  }

  constexpr int
  perm2prot(perm_t perm) noexcept
  {
    constexpr int p_read = PROT_READ;
    constexpr int p_write = PROT_WRITE;
    constexpr int p_rdwr = p_read | p_write;

    switch (perm) {
      case perm_t::RDONLY:
        return p_read;
      case perm_t::WRONLY:
        return p_write;
      case perm_t::RDWR:
        return p_rdwr;
      default:
        return p_read;
    }

    return p_rdwr;
  }

} // namespace mipc
