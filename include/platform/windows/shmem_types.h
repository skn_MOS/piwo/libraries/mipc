#pragma once
#include "shmem_native.h"

namespace mipc {
  // Identifier for shared object on windows is handle
  // to file mapping object.
  typedef shmem::shmem_native_t shobj_id_t;
} // namespace mipc
