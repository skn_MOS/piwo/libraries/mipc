#pragma once
#include "permission.h"

#include <windows.h>
#define INVALID_FD INVALID_HANDLE_VALUE

namespace mipc {

  typedef HANDLE fd_native_t;

  constexpr DWORD
  perm2generic(perm_t perm)
  {
    switch (perm) {
      case perm_t::RDONLY:
        return GENERIC_READ;
      case perm_t::WRONLY:
        return GENERIC_WRITE;
      case perm_t::RDWR:
        return GENERIC_READ | GENERIC_WRITE;
      default:
        return GENERIC_READ;
    }
  }

  constexpr DWORD
  perm2sharemode(perm_t perm)
  {
    constexpr DWORD def = FILE_SHARE_DELETE;

    switch (perm) {
      case perm_t::RDONLY:
        return def | FILE_SHARE_READ;
      case perm_t::WRONLY:
        return def | FILE_SHARE_WRITE;
      case perm_t::RDWR:
        return def | FILE_SHARE_READ | FILE_SHARE_WRITE;
      default:
        return def;
    }
  }

  constexpr DWORD
  perm2pageprot(perm_t perm)
  {
    switch (perm) {
      case perm_t::RDONLY:
        return PAGE_READONLY;
      case perm_t::WRONLY:
      case perm_t::RDWR:
        return PAGE_READWRITE;
      default:
        return PAGE_READONLY;
    }
  }

  constexpr DWORD
  perm2mapacc(perm_t perm)
  {
    switch (perm) {
      case perm_t::RDONLY:
        return FILE_MAP_READ;
      case perm_t::WRONLY:
        return FILE_MAP_WRITE;
      case perm_t::RDWR:
        return FILE_MAP_ALL_ACCESS;
      default:
        return FILE_MAP_READ;
    }
  }

} // namespace mipc
