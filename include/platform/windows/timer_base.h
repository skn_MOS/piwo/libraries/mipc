#pragma once
#include <windows.h>

namespace mipc {

constexpr HANDLE INVALID_TIMER_ID = NULL;

void timer_callback(PVOID lpParam, BOOLEAN TimerOrWaitFired);
}
