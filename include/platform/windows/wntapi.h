#pragma once

#include <windows.h>

typedef enum _SECTION_INFORMATION_CLASS
{
  SectionBasicInformation,
  SectionImageInformation
} SECTION_INFORMATION_CLASS;

typedef struct _SECTION_BASIC_INFORMATION
{
  PVOID Base;
  ULONG Attributes;
  LARGE_INTEGER Size;
} SECTION_BASIC_INFORMATION;

typedef DWORD(WINAPI *NTQUERYSECTIONPT)(HANDLE,
                                        SECTION_INFORMATION_CLASS,
                                        PVOID,
                                        ULONG,
                                        PULONG);
