#pragma once

#include <cstddef>
#include <windows.h>

#include "file_native.h"

static constexpr std::byte *INVALID_MAP = nullptr;

namespace mipc {
  inline void *
  host_map(fd_native_t fd, size_t size, mipc::perm_t prot) noexcept
  {
    return MapViewOfFile(fd, perm2mapacc(prot), 0, 0, size);
  }

  inline void
  host_unmap(void *addr, [[maybe_unused]] size_t size) noexcept
  {
    UnmapViewOfFile(addr);
  }
} // namespace mipc
