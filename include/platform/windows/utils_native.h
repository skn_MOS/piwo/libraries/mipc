#pragma once

#include <cstdint>

#include <errhandlingapi.h>

namespace mipc::utils {

  inline int64_t
  get_last_system_error()
  {
    return GetLastError();
  }

} // namespace mipc::utils
