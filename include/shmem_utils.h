#pragma once
#include "file_native.h"
#include "shmem.h"
#include "utils.h"

#include <algorithm>
#include <iterator>

namespace mipc::utils {
  mipc::shmem::shmem_t::meminfo_t
  json_parse_from_memory(const void *memory);

  mipc::shmem::shmem_t::meminfo_t
  json_parse_from_memory(const void *memory, size_t length);

  size_t
  get_file_size(mipc::fd_native_t fd) noexcept;

  template<typename IteratorType>
  constexpr auto
  find_json_end(IteratorType begin, IteratorType end)
  {
    using value_type =
      typename std::iterator_traits<IteratorType>::value_type;

    constexpr auto json_begin = static_cast<value_type>('{');
    constexpr auto json_end = static_cast<value_type>('}');

    size_t level = 0;

    begin = std::find(begin, end, json_begin);

    if (UNLIKELY(begin == end))
      return begin;

    while (begin != end) {
      if (*begin == json_begin)
        ++level;

      else if (*begin == json_end) {
        --level;
        if (level == 0) {
          ++begin;
          break;
        }
      }

      ++begin;
    }

    return begin;
  }

  template<typename SizeType, typename IteratorType>
  constexpr auto
  get_json_size(const IteratorType &begin, const IteratorType &end)
  {
    return static_cast<SizeType>(find_json_end(begin, end) - begin);
  }
} // namespace mipc::utils
