#pragma once

#include <cstdint>
#include <string>
#include <string_view>
#include <tuple>

#if defined(GetObject)
#  undef GetObject
#endif

#include <rapidjson/document.h>

#include "file_guard.h"
#include "mapping.h"
#include "shmem_native.h"
#include "utils.h"

#include "shmem_types.h"

namespace mipc::shmem {

  class shmem_t
  {
  public:
    typedef rapidjson::Document meminfo_t;

  private:
    meminfo_t meminfo;
    mmapping_t mmapping;
    shobj_id_t shobj;
    size_t header_size;

    shmem_t(meminfo_t &&meminfo,
            mmapping_t &&mmapping,
            shobj_id_t &&shobj,
            size_t header_size) noexcept FORCE_INLINE
      : meminfo(std::move(meminfo))
      , mmapping(std::move(mmapping))
      , shobj(std::move(shobj))
      , header_size(header_size)
    {}

    shmem_t(meminfo_t &&meminfo,
            mmapping_t &&mmapping,
            size_t header_size) noexcept FORCE_INLINE
      : meminfo(std::move(meminfo))
      , mmapping(std::move(mmapping))
      , shobj()
      , header_size(header_size)
    {}

  public:
    shmem_t() = default;

    shmem_t(shmem_t &&so)
      : meminfo{ std::move(so.meminfo) }
      , mmapping{ std::move(so.mmapping) }
      , shobj{ std::move(so.shobj) }
      , header_size{ so.header_size }
    {}

    shmem_t(const shmem_t &so) = delete;

    void
    operator=(shmem_t &&rhs)
    {
      if (UNLIKELY(this == &rhs))
        return;

      this->meminfo = std::move(rhs.meminfo);
      this->mmapping = std::move(rhs.mmapping);
      this->shobj = std::move(rhs.shobj);

      this->header_size = rhs.header_size;
    }

    void
    operator=(const shmem_t &rhs) = delete;

    ~shmem_t() = default;

    // Creates named shared memory in host os, maps and configures
    // it according to passed config file, and copies config
    // file into created shared memory,
    static shmem_t
    create(const char *name, const char *config_filename) noexcept;

    // Same as create, but fills headera with data under passed pointer
    // instead of contents of a file.
    // data parameter must be zero terminated.
    static shmem_t
    create_(const char *name,
            const char *header_data,
            size_t header_size) noexcept;

    static shmem_t
    create_(const char *name, mmapping_t &config_file) noexcept;

    // Opens shared memory and maps it current process address space.
    static shmem_t
    open(const char *name) noexcept;

    void
    close() noexcept;

    const meminfo_t &
    get_meminfo() const noexcept
    {
      return this->meminfo;
    }

    const auto &
    get_meminfo(const char *key) const noexcept
    {
      return this->meminfo[key];
    }

    auto
    get_mmapping() const noexcept
    {
      return this->mmapping.as_tuple();
    }

    auto
    get_payload() const noexcept
    {
      return this->mmapping.get_addr() + header_size;
    }

    auto
    get_header_size() const noexcept
    {
      return this->header_size;
    }

    bool
    is_valid() const noexcept
    {
      return this->mmapping.is_valid();
    }
  };

} // namespace mipc::shmem
