#pragma once
#include "fs.h"
#include "permission.h"

namespace mipc {

  inline bool
  fd_valid(fd_native_t fd)
  {
    return fd != INVALID_FD;
  }

  fd_native_t
  host_open(const char *file_name, perm_t perm) noexcept;

  fd_native_t
  host_create(const char *file_name, perm_t mode, perm_t access) noexcept;

  void
  host_close(fd_native_t fd) noexcept;

  void
  host_unlink(const char *file_name) noexcept;

  size_t
  host_filesize(fd_native_t fd) noexcept;

} // namespace mipc
