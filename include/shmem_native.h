#pragma once
#include <string>
#include <utility>

#include "file_native.h"
#include "permission.h"
#include "utils.h"

namespace mipc::shmem {

  class shmem_native_t
  {
  private:
    fd_native_t fd;

    shmem_native_t(fd_native_t fd) noexcept
      : fd(fd)
    {}

  public:
    shmem_native_t() noexcept
      : fd(INVALID_FD)
    {}
    shmem_native_t(const shmem_native_t &other) = delete;
    shmem_native_t(shmem_native_t &&other) noexcept;

    void
    operator=(const shmem_native_t &other) = delete;

    void
    operator=(shmem_native_t &&other) noexcept;

    ~shmem_native_t() noexcept;

    static shmem_native_t
    create(const char *name,
           size_t size,
           perm_t mode,
           perm_t access) noexcept;

    static shmem_native_t
    open(const char *name, perm_t perm) noexcept;

    int
    close() noexcept;

    int
    _close() noexcept;

    decltype(auto)
    get_fd() const noexcept
    {
      return this->fd;
    }

    void
    _set_fd(fd_native_t new_fd) noexcept
    {
      this->fd = new_fd;
    }

    bool
    is_valid() const noexcept
    {
      return fd_valid(this->fd);
    }

  private:
    static fd_native_t
    host_create(const char *name,
                size_t size,
                perm_t mode,
                perm_t access) noexcept;

    static fd_native_t
    host_open(const char *name, perm_t perm) noexcept;

    int
    host_close() noexcept;

  public:
    static int
    host_remove(const char *name) noexcept;

    static int
    host_close(fd_native_t fd) noexcept;
  };

} // namespace mipc::shmem
