#pragma once

#include <chrono>
#include <memory>
#include <thread>
#include <type_traits>

#include "timer_base.h"
#include "utils.h"

namespace mipc
{

class timer
{
#ifdef __WIN32
  friend void
  timer_callback(PVOID lpParam, BOOLEAN TimerOrWaitFired);
#endif

  static constexpr int POLLINF    = -1;
  static constexpr int EVENTS_CNT = 2;
  using event_val_t               = uint64_t;
  enum class event_id : uint8_t
  {
    TIMER = 0,
    RESET = 1,
  };

public:
  bool
  start(std::chrono::seconds      sec,
        std::chrono::milliseconds milisec,
        bool                      single_shot = false);

  bool
  stop();

  bool
  is_valid() const
  {
#ifdef __WIN32
    return hTimerQueue != INVALID_TIMER_ID;
#elif __linux__
    return pfd[utils::underlay_cast(event_id::TIMER)].fd != INVALID_TIMER_ID;
#endif
  }

  bool
  wait();

  bool
  is_active()
  {
    return active;
  }

  static unsigned int
  host_error()
  {
#ifdef __WIN32
    return GetLastError();
#elif __linux__
    return errno;
#endif
  }

  timer();
  timer(const timer&) = delete;
  // implement if needed
  timer(timer&&) = delete;
  ~timer();

private:
  void
  timer_handler();
  bool
  trigger_timer(std::chrono::seconds      sec,
                std::chrono::milliseconds milisec,
                bool                      single_shot = false);

#ifdef __WIN32
  HANDLE callback_events[EVENTS_CNT];
  HANDLE hTimer      = NULL;
  HANDLE hTimerQueue = NULL;

#elif __linux__
  struct pollfd pfd[EVENTS_CNT];
#endif
  bool active;
};

class timer_async
{
  using callback_t = void (*)(void);

public:
  timer_async(callback_t f)
    : user_handler(f)
  {
  }

  ~timer_async() { join(); }

  bool
  run(std::chrono::seconds sec, std::chrono::milliseconds milisec)
  {
    if (th.joinable() || !t.is_valid())
      return false;

    if (t.start(sec, milisec) == false)
      return false;

    th = std::thread(
      [this]()
      {
        while (t.wait())
        {
          this->user_handler();
        }
      });
    return true;
  }

  bool
  join()
  {
    if (t.stop() == false)
      return false;

    if (th.joinable())
      th.join();

    return true;
  }

  static unsigned int
  host_error()
  {
    return timer::host_error();
  }

private:
  timer       t;
  std::thread th;
  callback_t  user_handler;
};

} // namespace mipc
