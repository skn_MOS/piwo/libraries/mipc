#pragma once

#include "file_native.h"
#include "utils.h"

#include <cstdint>
#include <string_view>

#ifdef __WIN32

#include <windows.h>

#elif __linux__

#include <string_view>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/un.h>
#include <unistd.h>

#endif

namespace mipc::pipe {

  class conn_t
  {
  public:
#ifdef __WIN32
    constexpr static std::string_view system_pipe_path = "\\\\.\\pipe\\";
#elif __linux__
    constexpr static std::string_view system_pipe_path = "/tmp/";
#endif

  private:
    fd_native_t fd;

// *~* PLATFORM DEPENDENT VARIABLES *~* //
#ifdef __WIN32
    fd_native_t com_port;
    bool io_pending;
    OVERLAPPED overlapped_info;
#endif

// *~* PLATFORM DEPENDENT INITIALIZATION *~* //
#ifdef __WIN32
    conn_t(fd_native_t fd)
      : fd(fd)
    {
      this->com_port = CreateIoCompletionPort(fd, NULL, 1, 0);

      if (UNLIKELY(!com_port)) {
        mipc::host_close(fd);
        this->fd = INVALID_FD;
      }

      io_pending = false;
      overlapped_info = {};
    }
#elif __linux__
    conn_t(fd_native_t fd)
      : fd(fd)
    {}
#endif

  public:
// *~* PLATFORM DEPENDENT INITIALIZATION *~* //
#ifdef __WIN32
    conn_t()
      : fd(INVALID_FD)
      , com_port(INVALID_FD)
      , io_pending(false)
      , overlapped_info({})
    {}
#elif __linux__
    conn_t()
      : fd(INVALID_FD)
    {}
#endif

    conn_t(const conn_t &conn) = delete;
    conn_t(conn_t &&conn);

    void
    operator=(const conn_t &conn) = delete;
    void
    operator=(conn_t &&conn);

    ~conn_t() noexcept;

    static conn_t
    open(const char *name);

    int
    write(const char *data, size_t size);

    int
    read(char *buf, size_t size);

    // Reads n chars from pipe into buffer.
    // Returns count of unread chars, ie ret = n - total_chars_read.
    int
    readn(char *buf, size_t n);

    // Waits for io on its file descriptor.
    // Timeout value is in milliseconds.
    // Returns negative number when error occured, errno is set
    // accordingly, positive number when returned on data arrival and 0
    // when timedout.
    int
    wait(int timeout) noexcept;

    void
    close() noexcept;

    bool
    is_valid() const noexcept
    {
      return mipc::fd_valid(this->fd);
    }

    static conn_t
    _create_from(fd_native_t fd)
    {
      return { fd };
    }

    static unsigned int
    host_error() noexcept
    {
#ifdef __WIN32
      return GetLastError();
#elif __linux__
      return errno;
#endif
    }

  private:
    static fd_native_t
    host_open(const char *name);

#ifdef __WIN32
    bool
    enqueue_read() noexcept;
#elif __linux__
    // NOP
#endif
  };
} // namespace mipc::pipe
