#pragma once

namespace mipc {

  enum class perm_t
  {
    RDONLY,
    WRONLY,
    RDWR,
  };

}
