#pragma once

#include "file_native.h"
#include "pipe_conn.h"

#include <string_view>
#include <string>

#ifdef __WIN32

#elif __linux__

#include <string_view>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/un.h>
#include <unistd.h>

#endif

namespace mipc::pipe {

  class listener_t
  {
  public:
#ifdef __WIN32
    constexpr static std::string_view system_pipe_path = "\\\\.\\pipe\\";
#elif __linux__
    constexpr static std::string_view system_pipe_path = "/tmp/";
#endif

  private:
    fd_native_t fd;
#ifdef __WIN32
    std::string pipe_name;
#endif

    listener_t(fd_native_t fd)
      : fd(fd)
    {}

  public:
    listener_t()
      : fd(INVALID_FD)
    {}

    listener_t(const listener_t &conn) = delete;
    listener_t(listener_t &&conn);

    void
    operator=(const listener_t &conn) = delete;
    void
    operator=(listener_t &&conn);

    ~listener_t() noexcept;

    static listener_t
    create(const char *name, int max_con = 1);

    conn_t
    accept();

    bool
    is_valid() const noexcept
    {
      return mipc::fd_valid(this->fd);
    }

    static unsigned int
    host_error() noexcept
    {
#ifdef __WIN32
      return GetLastError();
#elif __linux__
      return errno;
#endif
    }

  private:
    fd_native_t
    host_accept();

    static fd_native_t
    host_create(const std::string &name, unsigned max_con = 1);
  };

} // namespace mipc::pipe
