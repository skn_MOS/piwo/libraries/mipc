#pragma once
#include <string_view>
#include <tuple>
#include <type_traits>

// Used to mark function as being mocked one.
#define MIPC_MOCK

#define LIKELY(x) __builtin_expect((x), 1)
#define UNLIKELY(x) __builtin_expect((x), 0)
#define FORCE_INLINE __attribute__((always_inline))

#include "utils_native.h"

namespace mipc::utils {

  template<class T>
  using utype = std::underlying_type_t<T>;

  template<class T>
  inline constexpr decltype(auto)
  underlay_cast(T e)
  {
    return static_cast<utype<T>>(e);
  }

  bool
  starts_with(std::string_view s, std::string_view prefix);

  inline int64_t
  get_last_system_error();

} // namespace mipc::utils
